import Vue from 'vue'
import Vuex from 'vuex'
import { getToken, setToken } from '@/utils/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tokenObject: getToken() || {}
  },
  mutations: {
    setToke (state, obj) {
      state.tokenObject = obj
      setToken(obj)
    }
  },
  getters: {},

  actions: {},
  modules: {}
})
