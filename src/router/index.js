import Vue from 'vue'
import VueRouter from 'vue-router'
import MyLogin from '@/views/MyLogin'
import LayOut from '@/views/LayOut'
import MyHome from '@/views/MyHome'
import MyAsk from '@/views/MyAsk'
import MyVideo from '@/views/MyVideo'
import MyMain from '@/views/MyMain'
import store from '@/store'
import { Toast } from 'vant'
import MyInfo from '@/views/MyHome/MyInfo'

Vue.use(Toast)
Vue.use(VueRouter)

const routes = [
  { path: '', redirect: '/layout/home' },
  { path: '/login', name: 'login', component: MyLogin },
  { path: '/info', name: 'info', component: MyInfo },

  {
    path: '/layout',
    name: 'layout',
    component: LayOut,
    children: [
      { path: 'home', name: 'MyHome', component: MyHome },
      { path: 'ask', name: 'MyAsk', component: MyAsk },
      { path: 'video', name: 'MyVideo', component: MyVideo },
      {
        path: 'main',
        name: 'MyMain',
        component: MyMain,
        meta: {
          needLogin: true
        }
      }
    ]
  }
]

const router = new VueRouter({
  routes
})
export default router

router.beforeEach((to, from, next) => {
  if (to.meta.needLogin) {
    if (store.state.tokenObject.token) {
      next()
    } else {
      Toast('请先登陆')
      next({
        name: 'login',
        query: {
          back: to.path
        }
      })
    }
  } else {
    next()
  }
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
