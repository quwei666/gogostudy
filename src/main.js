import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'amfe-flexible'
import './utils/vant'
import './style/base.less'
import moment from 'moment'
moment.locale('zh-cn')

// 声明一个全局的过滤器，用来处理时间
Vue.filter('getTiem', val => {
  // 把传递过来的绝对时间转换成相对时间再返回
  return moment(val).fromNow()
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
