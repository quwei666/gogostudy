export function setToken (token) {
  localStorage.setItem('tokenObj', JSON.stringify(token))
}

export function getToken () {
  return JSON.parse(localStorage.getItem('tokenObj'))
}
export function removeToken () {
  localStorage.removeItem('tokenObj')
}
