import axios from 'axios'
import store from '@/store'
import { Toast } from 'vant'
import router from '@/router'
import Vue from 'vue'
Vue.use(Toast)
const request = axios.create({
  baseURL: 'http://geek.itheima.net/'
})
export default request

// import store from '@/store'
// 请求拦截器
request.interceptors.request.use(
  config => {
    // 如果有token就帮你加token

    if (store.state.tokenObject.token && config.flag != true) {
      config.headers.Authorization = 'Bearer ' + store.state.tokenObject.token
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

// 响应拦截器
request.interceptors.response.use(
  res => {
    // 单独把data返回出来
    return res.data
  },
  async err => {
    if (err.response.status === 401) {
      try {
        let res = await request({
          method: 'put',
          url: '/v1_0/authorizations',
          headers: {
            Authorization: 'Bearer ' + store.state.tokenObject.refresh_token
          },
          flag: true
        })

        store.commit('setToke', {
          token: res.data.token,
          refresh_token: store.state.tokenObject.refresh_token
        })
        return request(err.config)
      } catch (error) {
        Toast('登陆过时请重新登录')
        router.push('/login')
      }
    }
    return Promise.reject(err)
  }
)
