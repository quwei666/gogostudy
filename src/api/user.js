import request from '@/utils/request'
import store from '@/store'

export function loginAPI (data) {
  return request({
    method: 'post',
    url: '/v1_0/authorizations',
    data
  })
}
export function userInfoAPi () {
  return request({
    method: 'get',
    url: '/v1_0/user/profile',
    headers: {
      Authorization: 'Bearer ' + store.state.tokenObject.token
    }
  })
}
export function tabsAPI () {
  return request({
    url: '/v1_0/user/channels'
  })
}
export function articlesAPI (params) {
  return request({
    url: '/v1_0/articles',
    params
  })
}
export function infoAPI (id) {
  return request({
    url: `/v1_0/articles/${id}`
  })
}
export function commentAPI (params) {
  return request({
    url: '/v1_0/comments',
    params
  })
}
export function replyAPI (params) {
  return request({
    url: '/v1_0/comments',
    params
  })
}
export function fieldAPI (data) {
  return request({
    url: '/v1_0/comments',
    method: 'post',
    data
  })
}
